/**
 * Functions configuration.
 */
export const functionsConfig = {
  whitelist: [
      'http://localhost:4200',
      'https://firestore-angular-starter-test.firebaseapp.com',
      'https://firestore-angular-starter.firebaseapp.com'
  ]
}
