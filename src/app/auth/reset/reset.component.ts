import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { MatchValidator } from '../sign-up/validators/match.validator';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {
  code: string;
  resetForm: any;

  constructor(    private authservice: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder
    ) { }
    get password() {
      return this.resetForm.get('password');
    }
    get confirmPassword() {
      return this.resetForm.get('confirmPassword');
    }
  ngOnInit() {
    this.createForm();
    this.route.queryParamMap.subscribe((params) => {
     this.code = params.get('oobCode');
    });
  }

  createForm() {

    this.resetForm = this.fb.group({
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      confirmPassword: ['', Validators.compose([Validators.minLength(6), Validators.required])],
    },
    { validator: MatchValidator.MustMatch('password', 'confirmPassword')}
    );
  }

  ResetPassword() {
      this.authservice.resetPassword(this.code , this.resetForm.get('confirmPassword').value).then();
  }

}
