import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';
import { MatchValidator } from './validators/match.validator';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})

export class SignUpComponent implements OnInit {
  signForm: any;
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  public affiliateNumber: string;

  constructor(
    public authService: AuthService,
    private fb: FormBuilder
  ) {

  }

  get email() {
    return this.signForm.get('email');
  }
  get password() {
    return this.signForm.get('password');
  }
  get confirmPassword() {
    return this.signForm.get('confirmPassword');
  }
  get sponsor() {
    return this.signForm.get('sponsor');
  }


  ngOnInit() { this.createForm(); }


  createForm() {
    this.signForm = this.fb.group({
      email: ['', Validators.compose([Validators.pattern(this.emailPattern), Validators.required])],
      /*
      Thrown when using a weak password (less than 6 chars) to create a new account or to update an existing account's password
      */
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      confirmPassword: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      sponsor: [this.affiliateNumber ? this.affiliateNumber : '', {
        validators: Validators.compose([Validators.minLength(7), Validators.maxLength(14),
        Validators.required])
      }
      ]
    },
      { validator: MatchValidator.MustMatch('password', 'confirmPassword') }
    );
  }
  async SignUp() {
    const { email, password, sponsor } = this.signForm.value;
    await this.authService.SignUp(email, password, sponsor);
  }
}
