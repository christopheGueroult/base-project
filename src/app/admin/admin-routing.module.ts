import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { HomeComponent } from './containers/home/home.component';
import { Route } from '../shared/services/route';

// Include route guard in routes array
const routes: Routes = [
  Route.withShellAuthAdmin([
    {path: '', component: HomeComponent}
  ])

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AdminRoutingModule { }
