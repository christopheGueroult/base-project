import { Component } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  config: any;

  constructor(
    private ngxUiLoaderService: NgxUiLoaderService
  ) {
    this.config = this.ngxUiLoaderService.getDefaultConfig();
  }
}
