import { Component, OnInit , Input } from '@angular/core';
import {ClientService} from '../../../clients/services/client.service';

@Component({
  selector: 'app-tableau',
  templateUrl: './tableau.component.html',
  styleUrls: ['./tableau.component.css'],
})
export class TableauComponent implements OnInit {
  @Input() headers: string[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
