import { NgModule } from '@angular/core';
import { DisableControlDirective } from './directives/disable-control.directive';
import { AuthBlockComponent } from './components/auth-block/auth-block.component';
import { FirestoreDatePipe } from './directives/firestore-date.directive';
import { TableauComponent } from './components/tableau/tableau.component';
import {CommonModule} from '@angular/common';
import { ButtonComponent } from './components/button/button.component';
import { StateColorDirective } from './directives/state-color.directive';




@NgModule({
  declarations: [
    DisableControlDirective,
    AuthBlockComponent,
    FirestoreDatePipe,
    TableauComponent,
    ButtonComponent,
    StateColorDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [DisableControlDirective, AuthBlockComponent, FirestoreDatePipe, TableauComponent, ButtonComponent, StateColorDirective]
})
export class SharedModule { }
